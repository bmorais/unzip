package br.com.rd.zip;

import static br.com.rd.util.Arquivos.apagandoLnk;
import static br.com.rd.util.Arquivos.validarMd5;
import static br.com.rd.util.Unzip.unzip;

import java.io.IOException;

import org.apache.log4j.Logger;

public class UnzipZilla {

	private static final Logger LOG = Logger.getLogger(UnzipZilla.class);

	public static void main(String[] args) throws IOException {
		LOG.info("Iniciando unzipZilla 1.5.0");

		if (validarMd5()) {
			unzip();
			apagandoLnk();
		}
		LOG.info("unzipZilla Finalizado!");
	}
}
