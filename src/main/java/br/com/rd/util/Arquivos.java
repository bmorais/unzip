package br.com.rd.util;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.log4j.Logger;

public class Arquivos {
	
	private Arquivos() {}

	private static final Logger LOG = Logger.getLogger(Arquivos.class);

	private static final String[] AGENTS_ZIP = { "C:\\TC-Util.zip", "7673bc8571a5c0e4518a6b4c41ed883e" };
	private static final String FOLDER = "C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\StartUp\\";
	private static String[] arquivos = { "inventario.lnk", "inventario.bat", "start-tcscanner.lnk",
			"start-tcscanner.bat", "start-tcprinter.lnk", "start-tcprinter.bat", "agent-inventory.lnk",
			"agent-inventory.bat" };

	public static void apagandoLnk() throws IOException {
		LOG.info("VALIDANDO ARQUIVOS");
		for (int i = 0; i < arquivos.length; i++) {
			String f = String.format("%s%s", FOLDER, arquivos[i]);
			if (new File(f).exists()) {
				Files.delete(Paths.get(f));
				LOG.info(String.format("DELETANDO ARQUIVO: %s", arquivos[i]));
			}
		}
	}

	public static boolean validarMd5() throws IOException {
		String md5 = "";
		try (InputStream is = Files.newInputStream(Paths.get(AGENTS_ZIP[0]))) {
			md5 = md5Hex(is);
			LOG.info(md5);
		}
		return !md5.isEmpty() && AGENTS_ZIP[1].equals(md5);
	}
}
